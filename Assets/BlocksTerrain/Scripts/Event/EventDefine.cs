﻿using System.Collections;
using System.Collections.Generic;

public delegate void LookupHandler(RaycastHitVoxelInfo hitInfo);
public delegate void MouseButtonDownHandler(int keyCode); // 0 - L  1 - R  2 - M
