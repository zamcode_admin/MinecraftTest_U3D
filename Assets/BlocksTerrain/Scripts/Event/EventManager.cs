﻿using System.Collections;
using System.Collections.Generic;

public class EventManager
{
    public static event LookupHandler LookupEvent;
    public static void SendLookupEvent(RaycastHitVoxelInfo hitInfo)
    {
        LookupEvent(hitInfo);
    }

    public static event MouseButtonDownHandler MouseButtonDownEvent;
    public static void SendMouseButtonDownEvent(int keyCode)
    {
        MouseButtonDownEvent(keyCode);
    }
}
