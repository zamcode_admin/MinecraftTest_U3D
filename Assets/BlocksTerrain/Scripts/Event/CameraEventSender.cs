﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEventSender : MonoBehaviour
{
    RaycastHitVoxelInfo lastRaycastHitVoxelInfo;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        lastRaycastHitVoxelInfo = Raycast();
        RaycastHitVoxelInfo raycastHitVoxelInfo = Raycast();
        // if (lastRaycastHitVoxelInfo == raycastHitVoxelInfo) {
        //     return;
        // }
        EventManager.SendLookupEvent(raycastHitVoxelInfo);
        lastRaycastHitVoxelInfo = raycastHitVoxelInfo;
    }

    private RaycastHitVoxelInfo Raycast()
    {
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, Const.SelectDistance))
        {
            Chunk chunk = hit.collider.GetComponent<Chunk>();
            if (chunk != null)
            {
                return new RaycastHitVoxelInfo(
                    chunk.PositionToVoxelIndex(hit.point, hit.normal, false),
                    chunk.PositionToVoxelIndex(hit.point, hit.normal, true),
                    new Index(chunk.index.x, chunk.index.y, chunk.index.z)
                );
            }
        }
        return null;
    }
}
