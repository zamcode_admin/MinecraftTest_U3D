﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Index
{
    public int x, y, z;

    public Index(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Index(Vector3 pos)
    {
        this.x = (int)pos.x;
        this.y = (int)pos.y;
        this.z = (int)pos.z;
    }
    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }
    public override string ToString()
    {
        return (x + "_" + y + "_" + z);
    }
    public bool Equals(Index target)
    {
        if (target == null)
        {
            return false;
        }
        return (((target.x - x) | (target.y - y) | (target.z - z)) == 0);
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    // public static bool operator ==(Index target1, Index target2)
    // {
    //     if (target2 == null)
    //     {
    //         return false;
    //     }
    //     return (((target1.x - target2.x) | (target1.y - target2.y) | (target1.z - target2.z)) == 0);
    // }
    // public static bool operator !=(Index target, Index target2)
    // {
    //     return !(target == target2);
    // }
}
