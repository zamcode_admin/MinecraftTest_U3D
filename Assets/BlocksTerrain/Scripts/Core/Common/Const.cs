public class Const {
    public const string CharacterName = "FirstPerson";
    public const ushort ChunkSideLength = 16;
    public const ushort VisibleDistance = 10;
    public const ushort VisibleDistanceSQ = VisibleDistance * VisibleDistance;
    public const ushort VisibleHeight = 3;
    public const ushort VoxelType_Air = 0;
    public const ushort VoxelType_Clod = 1;
    public const ushort VoxelType_Grass = 2;
    public const ushort VoxelType_Wood = 3;
    public const ushort VoxelType_Leaves = 4;
    public const float TreeRate = 0.01f;
    public const float SelectDistance = 5.0f;
    public const string SelectClubName = "selected block graphics";
}
