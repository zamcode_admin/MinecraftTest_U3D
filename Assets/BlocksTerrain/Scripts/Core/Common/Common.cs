﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction {
    forward = 0,
    back,
    up,
    down,
    right,
    left,
}

public enum Facing {
    forward = 0,
    back,
    up,
    down,
    right,
    left,
}

public enum Transparency {
    solid, semiTransparent, transparent
}

public enum ColliderType {
    cube, mesh, none
}

public class RaycastHitVoxelInfo
{
    public Index index;
	public Index adjacentIndex;
	public Index chunkIndex;
    public RaycastHitVoxelInfo(Index index, Index adjacentIndex, Index chunkIndex)
    {
        this.index = index;
        this.adjacentIndex = adjacentIndex;
        this.chunkIndex = chunkIndex;
    }
}