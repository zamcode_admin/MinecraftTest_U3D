﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocksTerrain : MonoBehaviour
{
    public static BlocksTerrain Instance { get; private set; }
    // public GameObject ChunkObject { get; set; }
    public GameObject ChunkObject;
    public ChunkManager ChunkManager { get; private set; }
    public ushort Seed { get; private set; }
    private Index lastIndex;
    public void Awake()
    {
        Instance = this;
        ChunkManager = new ChunkManager();
        Seed = (ushort)Random.Range(ushort.MaxValue, ushort.MaxValue);
    }
    public void Start()
    {
        GenerateMap();
    }
    public void Update()
    {
        // GenerateMap(); // TODO 处理character下坠的过程
    }
    private void GenerateMap()
    {
        GameObject character = GameObject.Find(Const.CharacterName);
        try
        {
            Index index = ChunkManager.PositionToChunkIndex(character.transform.position);
            if (!index.Equals(lastIndex))
            {
                ChunkManager.SpawnChunks(index.x, index.y, index.z);
                lastIndex = index;
            }
        }
        catch (System.Exception)
        {
            Debug.LogError("BlocksTerrain Generate Map Can't Find Character" + character);
        }
    }
}
