﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class VoxelTypeManager : MonoBehaviour
{
    public static VoxelTypeManager Instance {get; private set;}
    // public Dictionary<ushort, GameObject> Voxels;
    public List<GameObject> Voxels;

    private VoxelTypeManager() {
    }

    private VoxelTypeManager(string voxelPath) {
    }

    public void Awake() {
        // Voxels = new List<GameObject>();
        Instance = this;
    }

    public VoxelType GetVoxelType(ushort voxelTypeID) {
        if (voxelTypeID == ushort.MaxValue)	voxelTypeID = 0;
        try {
            if (Voxels[voxelTypeID] == null) {
                return null;
            }else{
                return Voxels[voxelTypeID].GetComponent<VoxelType>();
            }
        } catch (System.Exception) {
            Debug.LogWarning("GetVoxelType " + voxelTypeID);
            return null;
        }
        // if (Voxels.ContainsKey(voxelTypeID)) {
        //     return Voxels[voxelTypeID].GetComponent<VoxelType>();
        // } else {
        //     return null;
        // }
	}

    // private void InitVoxelTypes() {
    //     string[] files = System.IO.Directory.GetFiles("Assets/BlocksTerrain/Prefabs/Voxel/");
    //     if (files == null || files.Length == 0) {
    //         return;
    //     }
    //     foreach (string filePath in files) {
    //         if (System.IO.Path.GetExtension(filePath) == ".prefab") {
    //             try {
    //                 Voxels[(Regex.Matches(filePath, @"(\d)")[(ushort)0])] = AssetDatabase.LoadAssetAtPath(filePath, typeof(Object));
    //             }catch (System.Exception) {
    //             }
    //         }
    //     }
    // }
}
