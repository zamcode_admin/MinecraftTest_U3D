﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelType : MonoBehaviour {
    public string VName;
	public bool Visiblity;
	public bool VCustomMesh;
	public Vector2[] VTexture;
	public Transparency VTransparency;
	public ColliderType VColliderType;
	public int VSubmeshIndex;
	// public MeshRotation VRotation;
    // void Start()
    // {        
    // }
    // void Update()
    // {   
    // }

    public Vector2 GetTextureOffset(Facing facing) {
        try {
            return VTexture[(int)facing];
        } catch (System.Exception) {
            return (VTexture.Length > 0) ? VTexture[0] : new Vector2(0.0f, 0.0f);
        }
    }
}

